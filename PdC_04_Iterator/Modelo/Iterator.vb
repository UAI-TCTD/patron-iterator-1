﻿Public MustInherit Class Iterator
    Public MustOverride Sub First()

    Public MustOverride Sub Siguiente()
    Public MustOverride Sub IsDone()

    Public MustOverride Sub Current()

    
End Class
