﻿Public Class ConcreteCollection
    Inherits Collection


    Private _listaItems As List(Of Item) = New List(Of Item)
    Private _item As Item
    Private _posicion As Integer
    Private _count As Integer



    Public Overrides Function CreateIterator() As IteratorEjemplo
        Return New ConcreteIteratorEjemplo(Me)
    End Function
    Public Overrides Sub Agregar(_item As Item)
        _listaItems.Add(_item)
    End Sub

    Public Overrides Function Index(_index As Integer) As Item
        _item = _listaItems(_index)
        _posicion = _index
        Return _item
    End Function


    Public Overrides Function Count() As Integer
        Return _listaItems.Count
    End Function

    Public Overrides Function Posicion() As Integer
        Return _posicion
    End Function
End Class


