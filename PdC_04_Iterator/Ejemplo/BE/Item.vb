﻿Public Class Item

    Private _nombre As String
    Public Property Nombre() As String
        Get
            Return _nombre
        End Get
        Set(ByVal value As String)
            _nombre = value
        End Set
    End Property

    Public Sub New(_nom As String)
        Nombre = _nom
    End Sub

    Public Overrides Function ToString() As String
        Return Me.Nombre

    End Function

End Class
