﻿Public MustInherit Class Collection
    Public MustOverride Function CreateIterator() As IteratorEjemplo
    Public MustOverride Sub Agregar(_item As Item)
    Public MustOverride Function Index(_index As Integer) As Item
    Public MustOverride Function Count() As Integer
    Public MustOverride Function Posicion() As Integer


End Class
