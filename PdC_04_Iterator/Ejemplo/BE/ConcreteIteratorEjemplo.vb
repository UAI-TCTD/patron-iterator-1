﻿Public Class ConcreteIteratorEjemplo
    Inherits IteratorEjemplo

    Private _aggregate As Collection = New ConcreteCollection
    Private _item As Item
    Public Overrides Function CurrentItem() As Item
        Return _item
    End Function

    Public Overrides Sub First()
        _item = _aggregate.Index(0)
    End Sub

    Public Overrides Function IsDone() As Boolean
        If _aggregate.Count = (_aggregate.Posicion + 1) Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Overrides Sub Siguiente()
        _item = _aggregate.Index(_aggregate.Posicion + 1)
    End Sub
    Public Sub New(_agregado As Collection)
        _aggregate = _agregado
    End Sub
End Class
