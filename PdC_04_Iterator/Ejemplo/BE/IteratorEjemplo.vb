﻿Public MustInherit Class IteratorEjemplo

    Public MustOverride Sub First()
    Public MustOverride Sub Siguiente()
    Public MustOverride Function IsDone() As Boolean
    Public MustOverride Function CurrentItem() As Item

End Class
