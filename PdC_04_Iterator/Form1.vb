﻿Public Class Form1
    Private _collection As Collection = New ConcreteCollection
    Private _list As List(Of Item) = New List(Of Item)
    Dim _iterador As IteratorEjemplo = _collection.CreateIterator()


    Private Sub Listar()
        Me.ListBox1.DataSource = Nothing
        Me.ListBox1.DataSource = _list
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click

        Dim _item As Item = New Item(TextBox1.Text)

        _collection.Agregar(_item)
        _list.Add(_item)

        Listar()
    End Sub

    Private Sub btnPrimero_Click(sender As Object, e As EventArgs) Handles btnPrimero.Click
        _iterador.First()
        txtPrimero.Text = _iterador.CurrentItem.Nombre
        TextBox2.Text = _iterador.CurrentItem.Nombre
    End Sub

    Private Sub btnSiguiente_Click(sender As Object, e As EventArgs) Handles btnSiguiente.Click

        If _iterador.IsDone = False Then
            _iterador.Siguiente()
            TextBox2.Text = _iterador.CurrentItem.Nombre
        End If
    End Sub
End Class
